import { Users } from '../entity/users.entity'
import { Account } from '../entity/account.entity'
import { Request, Response } from 'express'
import { myDataSource } from '../config/connectDB'
import bcrypt, { hash } from "bcrypt";
import { IsNull } from 'typeorm';

const UserRepository = myDataSource.getRepository(Users)
const AccountRepository = myDataSource.getRepository(Account)
const saltRounds = 10
export const register = async (req: Request, res: Response) => {
    const email = req.body.email
    const password = req.body.password
    const fullName = req.body.fullName
    if(email.length == 0 || password.length == 0 || fullName.lengh == 0) {
        res.status(400).send('please enter information ')
    }
    const hashPassword = await bcrypt.hash(password , saltRounds)
    const user = new Users();
    user.fullName = fullName
    user.email = email
    user.password = hashPassword
    const account = new Account();
    account.email = email
    account.password = hashPassword
    account.user = user
    const saveUser = await UserRepository.manager.save(user)
    const saveAccount = await AccountRepository.manager.save(account)
    if (saveUser && saveAccount) {
        res.status(200).send("register successfully")
    }
}

export const login = async (req: Request, res: Response) => {
    const email = req.body.email;
    const password = req.body.password
    if (email.length == 0 || password.length == 0) {
        res.send("enter your email and passsword")
    }
    const user = await UserRepository.findOne({
        where : {email}
    }) 
    if(!user) { 
        res.send("wrong email")
    }
    if(user?.password != null) { 
    const isValidate = await bcrypt.compare(password, user.password)
    if(!isValidate) { 
        res.send("wrong password")
    }
    }
    return res.send("login suscess")
}