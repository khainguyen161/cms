"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
import { Users } from './entity/users.entity';
var connectDB_1 = require("./config/connectDB");
var app = express();
app.use(express.json());
var port = process.env.PORT || 3000;
// createConnection()
connectDB_1.myDataSource
    .initialize()
    .then(function () {
    console.log("connect success!");
})
    .catch(function (err) {
    console.log("connect failure!" + err.message);
});
app.get('/test', function (req, res) {
    res.json({
        data: "test done"
    });
});
app.listen(port, function () {
    console.log("sever is running on port " + port);
});
