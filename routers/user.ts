import {Router}from 'express'
import {register,login} from '../service/user.service'
// import userService from '../service/user.service'

const router : Router = Router()

// get all users 
router.post('/register',register)
router.post('/login',login)

export default router