import  express ,{Response ,Request} from 'express';
import { Users } from './entity/users.entity';
import {myDataSource} from './config/connectDB'
import router from './cms/routers/user' 
const app = express();
app.use(express.json());

const port = process.env.PORT || 3000;

// createConnection()
myDataSource
.initialize()
  .then(()=> { 
    console.log("connect success!");
  })
  .catch(err => {
    console.log("connect failure!" + err.message);
    
  })

//set all routes 
app.use('/',router);
app.listen(port , () :void => {
    console.log("sever is running on port " + port);
    
});