import {Entity ,PrimaryGeneratedColumn , OneToOne , Column , JoinColumn} from 'typeorm'
import {Users} from './users.entity'
@Entity()
export class Account { 
    @PrimaryGeneratedColumn()
    id : number

    @Column()
    email : string

    @Column()
    password : string 

    @OneToOne(() => Users ) 
    @JoinColumn( {name : "userId"} )
    user : Users   
}

