import {Entity, Column,PrimaryGeneratedColumn, OneToOne} from 'typeorm'

@Entity()
export class Users { 
    @PrimaryGeneratedColumn()
    id : number

    @Column()
    email : string

    @Column()
    fullName : string 
    
    @Column()
    password : string
}